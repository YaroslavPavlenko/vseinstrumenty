<?php
namespace Entity;

use Repository\Order as RepositoryOrder;
require_once '../repository/Order.php';
use Repository\ProductsByOrder as RepositoryProductsByOrder;
require_once '../repository/ProductsByOrder.php';
use Repository\Product as RepositoryProduct;
require_once '../repository/Product.php';
require_once '../entity/Entity.php';

class Order extends Entity
{
    protected $params = [
        'id' => [
            'title' => 'ID'
        ],
        'user_id' => [
            'title' => 'ID пользователя'
        ],
        'status' => [
            'title' => 'Статус'
        ]
    ];

    private $id;
    private $user_id;
    private $status;

    public const STATUS_NEW = 1;// новый
    public const STATUS_PAID = 2;// оплачено


    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getUserId(): int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }



    public static function create(array $productIds): Order
    {
        $repositoryOrder = new RepositoryOrder();
        $orderData = $repositoryOrder->insert([
            'user_id' => 1,
            'status' => self::STATUS_NEW
        ]);

        /** @var Order $order */
        $order = self::init($orderData);
        $insertData = [];
        $repositoryProductsByOrder = new RepositoryProductsByOrder();
        $orderId = $order->getId();

        foreach ($productIds as $productId) {
            $insertData[] = [
                'order_id' => (int) $orderId,
                'product_id' => (int) $productId
            ];
        }

        $repositoryProductsByOrder->multipleInsert($insertData);

        return $order;
    }

    public function update(): array
    {
        $repositoryOrder = new RepositoryOrder();
        return $repositoryOrder->update($this->toArray());
    }

    public static function load(int $orderId): Order
    {
        $repositoryOrder = new RepositoryOrder();
        $orderData = $repositoryOrder->select($orderId);
        return self::init($orderData);
    }

    public function getTotalSumm(): float
    {
        $repositoryProductsByOrder = new RepositoryProductsByOrder();
        $productIds = $repositoryProductsByOrder->selectProductIdsByOrderId($this->getId());

        $repositoryProduct = new RepositoryProduct();
        $products = $repositoryProduct->selectByIds($productIds);

        $productsSumm = 0.0;

        foreach ($products as $product) {
            $productsSumm += (float) $product['price'];
        }

        return $productsSumm;
    }

}