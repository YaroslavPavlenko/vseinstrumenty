<?php

namespace Entity;


class Entity
{
    protected $params = [];

    protected static function init(array $data): Entity
    {
        $entity = new static();

        foreach ($data as $paramName => $paramValue) {
            $methodName = 'set' . self::getMethodByParamName($paramName);
            $entity->$methodName($paramValue);
        }

        return $entity;
    }

    protected function toArray(): array
    {
        $array = [];

        foreach ($this->params as $paramName => $paramOptions) {
            $methodName = 'get' . self::getMethodByParamName($paramName);
            $value = $this->$methodName();

            switch ($paramOptions['type']) {
                case 'string':
                    $value = (string) $value;
                    break;
                case 'float':
                    $value = (float) $value;
                    break;
                default:
                    $value = (int) $value;
            }

            $array[$paramName] = $value;
        }

        return $array;
    }

    private static function getMethodByParamName(string $paramName): string
    {
        $words = explode('_', $paramName);

        $words = array_map(function ($word) {
            return ucfirst(strtolower($word));
        },$words);

        return implode('', $words);
    }
}