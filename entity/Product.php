<?php
namespace Entity;

use Repository\Product as RepositoryProduct;
require_once '../repository/Product.php';
require_once '../entity/Entity.php';

class Product extends Entity
{
    protected $params = [
        'name' => [
            'title' => 'Название',
            'type' => 'string'
        ],
        'price' => [
            'title' => 'Цена',
            'type' => 'float'
        ]
    ];

    private $name;
    private $price;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    public static function getAll(): array
    {
        $repositoryProduct = new RepositoryProduct();
        return $repositoryProduct->selectAll();
    }

    public static function createDefault($count): bool
    {
        $defaultProductData = [
            'name' => 'product',
            'price' => 100.00,
        ];

        $productsData = [];

        for ($i = 1; $i <= $count; $i++) {
            $productsData[] = $defaultProductData;
        }

        $repositoryProduct = new RepositoryProduct();
        return $repositoryProduct->multipleInsert($productsData);
    }
}