<?php

namespace repository;

use repository\DataBase;
require_once 'DataBase.php';
use Entity\Order as EntityOrder;
require_once '../entity/Order.php';
require_once '../repository/Repository.php';

class Order extends Repository
{
    protected $tableName = 'orders';
    protected $params = [
        'user_id' => 'ID пользователя',
        'status' => 'Статус',
    ];

}