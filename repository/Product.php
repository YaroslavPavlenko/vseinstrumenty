<?php

namespace repository;

use repository\DataBase;
require_once 'DataBase.php';
require_once '../repository/Repository.php';

class Product extends Repository
{
    protected $tableName = 'products';
    protected $params = [
        'name' => 'Название',
        'price' => 'Цена',
    ];

}