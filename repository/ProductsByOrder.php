<?php

namespace repository;

use repository\DataBase;
require_once 'DataBase.php';
require_once '../repository/Repository.php';

class ProductsByOrder extends Repository
{
    protected $tableName = 'products_by_order';
    protected $params = [
        'product_id' => 'ID товара',
        'order_id' => 'ID заказа',
    ];

    public function selectProductIdsByOrderId(int $orderId)
    {
        $query = "SELECT product_id FROM $this->tableName WHERE order_id = $orderId;";
        $instance = DataBase::getInstance();
        $productsByOrderId = $instance->query($query);

        $productIds = array_map(function ($value) {
            return $value['product_id'];
        }, $productsByOrderId);

        return $productIds;
    }

}