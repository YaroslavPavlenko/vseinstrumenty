<?php

namespace repository;


class Repository
{
    protected $params = [];

    public function insert(array $values): array
    {
        $queryValues = $this->getInsertStringQuery($values);

        $params = implode(', ', array_keys($this->params));
        $query = "INSERT INTO $this->tableName ($params) VALUES $queryValues RETURNING *;";

        $instance = DataBase::getInstance();
        return $instance->insert($query);
    }

    public function multipleInsert(array $data): bool
    {
        $queries = [];

        foreach ($data as $values) {
            $queries[] = $this->getInsertStringQuery($values);
        }

        $queries = implode(',', $queries);
        $params = implode(', ', array_keys($this->params));

        $query = "INSERT INTO $this->tableName ($params) VALUES $queries;";

        $instance = DataBase::getInstance();
        $result = $instance->multipleInsert($query);

        return $result === PGSQL_COMMAND_OK;
    }

    public function select(int $id): array
    {
        $query = "SELECT * FROM $this->tableName WHERE id = $id;";
        $instance = DataBase::getInstance();
        return array_shift($instance->query($query));
    }

    public function selectAll(): array
    {
        $query = "SELECT * FROM $this->tableName;";
        $instance = DataBase::getInstance();
        return $instance->query($query);
    }

    public function selectByIds(array $productIds): array
    {
        $productIdsSelectIn = 'IN('.implode(',', $productIds). ')';
        $query = "SELECT * FROM $this->tableName WHERE id $productIdsSelectIn";
        $instance = DataBase::getInstance();
        return $instance->query($query);
    }

    public function update(array $data): array
    {
        $query = $this->arrayToUpdateQuery($data);
        $instance = DataBase::getInstance();
        return $instance->query($query);
    }

    public function getInsertStringQuery(array $data): string
    {
        $result = [];

        foreach (array_keys($this->params) as $param) {
            $value = $data[$param];

            if (is_string($value)) {
                $value = "'" . $value . "'";
            }

            $result[] = $value;
        }

        $result = '(' . implode(', ', $result) . ')';
        return $result;
    }

    public function arrayToUpdateQuery(array $data): string
    {
        $id = $data['id'];
        $params = [];

        foreach (array_keys($this->params) as $param) {
            $value = $data[$param];

            if (is_string($value)) {
                $value = "'" . $value . "'";
            }

            $params[] = "$param = $value";
        }

        $params = implode(', ', $params);

        return "UPDATE $this->tableName SET $params WHERE id = $id;";
    }
}