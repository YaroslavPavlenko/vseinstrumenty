<?php

namespace repository;


class DataBase
{
    private static $pgConnect;
    /**
     * @var DataBase
     */
    private static $instance;

    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$pgConnect = pg_connect('host=localhost dbname=test user=postgres password=1234qwer');
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public function insert(string $query): array
    {
        $queryResult = pg_query(static::$pgConnect, $query);
        return pg_fetch_assoc($queryResult);
    }

    public function multipleInsert(string $query)
    {
        $queryResult = pg_query(static::$pgConnect, $query);
        return pg_result_status($queryResult);
    }

    public function query(string $query): array
    {
        $queryResult = pg_query(static::$pgConnect, $query);
        $result = [];

        while ($row = pg_fetch_assoc($queryResult)) {
            $result[] = $row;
        }

        return $result;
    }

}