<?php
namespace Service;

use Entity\Product as EntityProduct;
require_once '../entity/Product.php';
use Entity\Order as EntityOrder;
require_once '../entity/Order.php';

class Order
{

    public function create($productIds): int
    {
        $order = EntityOrder::create($productIds);

        return $order->getId();
    }

    public function pay(float $paySumm, int $orderId)
    {
        $order = EntityOrder::load($orderId);
        $productsSumm = $order->getTotalSumm();

        if ($paySumm !== $productsSumm || $order->getStatus() !== EntityOrder::STATUS_NEW) {
            return false;
        }

        if (!$this->sendQueryYandex()) {
            return false;
        }

        $order->setStatus(EntityOrder::STATUS_PAID);
        $order->update();

        return true;
    }

    private function sendQueryYandex()
    {
        $myCurl = curl_init();
        curl_setopt_array($myCurl, [
            CURLOPT_URL => 'http://ya.ru/',
            CURLOPT_RETURNTRANSFER => true
        ]);
        curl_exec($myCurl);
        $responseCode = curl_getinfo($myCurl, CURLINFO_RESPONSE_CODE);

        return $responseCode === 200;
    }
}