<?php
namespace Service;

use Entity\Product as EntityProduct;
require_once '../entity/Product.php';

class Product
{

    public function makeDefaultProducts(): bool
    {
        return EntityProduct::createDefault(20);
    }

    public function getAllProducts(): array
    {
        return EntityProduct::getAll();
    }

}