<?php
namespace Controller;

use Service\Order as ServiceOrder;
require_once '../service/Order.php';

class Order
{
    public function __construct(){}

    /**
     * Пример: /order/save/1,2,3,4,5
     * @param $params
     */
    public function save($params): void
    {
        try {
            $productIds = explode(',', $params[0]);

            $serviceOrder = new ServiceOrder();
            $orderId = $serviceOrder->create($productIds);

            $response = [
                'status' => 'success',
                'result' => $orderId
            ];
        } catch (\Exception $e) {
            $response = ['status' => 'error'];
        }

        echo json_encode($response);
    }

    /**
     * Пример: /order/pay/1500,00/29
     * @param $params
     */
    public function pay($params): void
    {
        try {
            [$summ, $orderId] = $params;

            $serviceOrder = new ServiceOrder();
            $result = $serviceOrder->pay($summ, $orderId);

            $response = [
                'status' => 'success',
                'result' => $result
            ];
        } catch (\Exception $e) {
            $response = ['status' => 'error'];
        }

        echo json_encode($response);
    }
}