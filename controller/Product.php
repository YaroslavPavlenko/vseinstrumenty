<?php
namespace Controller;

use Service\Product as ServiceProduct;
require_once '../service/Product.php';

class Product
{
    public function __construct(){}

    /**
     * Пример: /product/default
     */
    public function default(): void
    {
        try {
            $serviceProduct = new ServiceProduct();
            $result = $serviceProduct->makeDefaultProducts();

            $response = ['status' => 'error'];

            if ($result) {
                $response['status'] = 'success';
            }
        } catch (\Exception $e) {
            $response = ['status' => 'error'];
        }

        echo json_encode($response);
    }

    /**
     * Пример: /product/list
     */
    public function list(): void
    {
        try {
            $serviceProduct = new ServiceProduct();
            $products = $serviceProduct->getAllProducts();

            $response = [
                'status' => 'success',
                'result' => $products
            ];
        } catch (\Exception $e) {
            $response = ['status' => 'error'];
        }

        echo json_encode($response);
    }

}