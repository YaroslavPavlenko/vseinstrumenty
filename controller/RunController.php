<?php

namespace Controller;

use Controller\Product;
use Controller\Order;

class RunController
{

    /**
     * RunController constructor.
     * @param $requestParams
     */
    public function __construct($requestParams)
    {
        $requestParams = explode('/', $requestParams);
        [,$controller, $action] = $requestParams;

        if (!($controller && $action)) {
            echo 'Переданы не верные параметры';
            die;
        }

        $controllerName = strtolower($controller);
        $actionName = strtolower($action);

        $controllerFile = ucfirst($controllerName) . '.php';

        require_once $controllerFile;
        $objectController = null;

        switch ($controllerName) {
            case 'product':
                $objectController = new Product();
                break;
            case 'order':
                $objectController = new Order();
                break;
            default:
                echo 'Данные не найдены';
                break;
        }

        unset(
            $requestParams[0],
            $requestParams[1],
            $requestParams[2]
        );

        $objectController->$actionName(array_values($requestParams));
    }
}